import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import HomeScreen from "./src/homeScreen";
import CountMoviment from "./src/countMoviment";
import Carrossel from "./src/carrossel";

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Count" component={CountMoviment} />
        <Stack.Screen name="Carrossel" component={Carrossel} />
      </Stack.Navigator>
    </NavigationContainer>
    
  );
}