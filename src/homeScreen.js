import React from "react";
import { View, Button } from "react-native";
import { useNavigation } from "@react-navigation/native";


const HomeScreen = () => {
    const navigation = useNavigation();

    const handleCount = () => {
        navigation.navigate("Count")
    }

        const handleCarrossel = () => {
            navigation.navigate("Carrossel");
    }
    
        return(
            <View>
                <Button title="Ver contador" onPress={handleCount}/>
                <Button title="Ir para Aba de imagens" onPress={handleCarrossel} color="blue"/>

            </View>
        )
};
export default HomeScreen;