import React, {useRef, useState, useEffect} from "react";
import { View, Dimensions, PanResponder, Button, Text } from "react-native";

const CountMoviment = () =>{

    const [count,setCount] = useState(0);
    const screenHeight = Dimensions.get("window").height
    const gestureThreshold = screenHeight * 0.25;

    const panResponder = useRef(
        PanResponder.create({
            onStartShouldSetPanResponder:()=>true,
            onPanResponderMove:(event,gestureState)=>{},
            onPanResponderRelease:(event,gestureState)=>{
                if(gestureState.dy < -gestureThreshold){
                    setCount((prevCount)=>prevCount+1)
                    
                }
                else if(gestureState.dy >  gestureThreshold){
                    setCount((prevCount)=>prevCount-1)
                    
                }
                
            }
        })
    ).current;

    return(
        <View {...panResponder.panHandlers} style={{ flex: 1, alignItems: "center", justifyContent: "center"}}>
            <Text> Quantas mulheres o japones pega escondido:{count}</Text>
        </View>
    )
}
export default CountMoviment;