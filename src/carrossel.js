import React , {useRef, useState, useEffect} from "react";
import { View, Text, Dimensions, PanResponder, Button, Image, StyleSheet } from "react-native";

const Carrossel = () => {
    const [count, setCount] = useState(0);
    const screenWidth = Dimensions.get("window").width;
    const gestureThreshold = screenWidth * 0.40;

    const img = [
        'https://via.placeholder.com/600/771796',
        'https://via.placeholder.com/600/24f355',
        'https://via.placeholder.com/600/92c952',
    ];

    useEffect(() => {
        console.log(count);
      },[count]);

    const panResponder = useRef(
        PanResponder.create({
            onMoveShouldSetPanResponder: ()=>true,
            onPanResponderMove:(event,gestureState)=>{},
            onPanResponderRelease:(event, gestureState)=>{
                if(gestureState.dx < -gestureThreshold){
                    setCount((prevCount)=>prevCount<img.length-1?prevCount+1:0)
                }
                else if(gestureState.dx > -gestureThreshold){
                    setCount((prevCount)=>prevCount>0?prevCount-1:img.length-1)
                }
            }
        })
    ).current;

    return(
        <View {...panResponder.panHandlers} style={styles.container}>
            <Text>Carrossel de imagens</Text>
            <Image style={styles.image} source={{uri: img[count]}} />
        </View>
    );
}

const styles = StyleSheet.create({
    container:{
        flex:1, 
        alignItems:'center', 
        justifyContent:'center'
    },
    image:{
        width: 300,
        height: 300,
        backgroundColor: 'lightgrey',
        borderRadius: 15
    }

});

export default Carrossel;
